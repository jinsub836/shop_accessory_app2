import 'dart:math';

import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:shop_accessory_app/component/component_goods_item.dart';
import 'package:shop_accessory_app/config/config_api.dart';
import 'package:shop_accessory_app/model/goods_item.dart';
import 'package:shop_accessory_app/pages/page_goods_detail.dart';
import 'package:gif_view/gif_view.dart';
import 'package:shop_accessory_app/config/config_api.dart';


class PageIndex extends StatefulWidget {
  const PageIndex({
    super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}


class _PageIndexState extends State<PageIndex> with TickerProviderStateMixin{

  String searchValue = '';
  final List<String> _suggestions = ['귀걸이', '목걸이', '반지', '팔찌', '손목 시계', '브로치', '부토니에르', '벨트', '머리띠'];
  ApiService apiService = ApiService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
        EasySearchBar(
        title: const Text('Example'
        ,style:TextStyle
            (color: Colors.white,fontSize: 20,fontFamily: 'Lemon', fontWeight:FontWeight.w200),
        ),
        backgroundColor: Colors.brown,
        onSearch: (value) => setState(() => searchValue = value),
        actions: [
          IconButton(icon: const Icon(Icons.person), onPressed: () {})
        ],
        iconTheme: IconThemeData(color: Colors.white),
        suggestions: _suggestions
    ),drawer: Drawer(
        child: ListView(
            padding: EdgeInsets.zero,
            children: [
              Container(
                height: 100,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.brown,
                ),
                child: Text('메뉴',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontFamily:'Lemon'
                ),),
              )),
              ListTile(
                  title: Text('회원 정보'),
                  onTap: () => Navigator.pop(context)
              ),
              ListTile(
                  title: Text(''),
                  onTap: () => Navigator.pop(context)
              )
            ]
        )
    ),
      body: _buildBody(), );

  }


  Widget _buildBody() {
    return FutureBuilder<List<GoodsItem>>(
      future: apiService.getList(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('에러: ${snapshot.error}'));
        } else {
          List<GoodsItem> goodsList = snapshot.data!;
          return SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Center(
                    child: Text(
                      ' cold, smooth & tasty.',
                      style: TextStyle(
                        fontSize: 25,
                        fontFamily: 'Lemon',
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: GifView.asset(
                    'assets/dualipa.gif',
                    controller: GifController(loop: false),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Image.asset('assets/bigsale.png'),
                ),
                Container(
                  width: 400,
                  child: GridView.builder(
                    physics: ScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: goodsList.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 2 / 3,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      return ComponentGoodsItem(
                        goodsItem: goodsList[index],
                        callback: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PageGoodsDetail(
                                  itemNum: goodsList[index])));
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
