import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:shop_accessory_app/component/component_goods_item.dart';
import 'package:shop_accessory_app/model/goods_item.dart';

class PageGoodsDetail extends StatefulWidget {
  const PageGoodsDetail({super.key,
  required this.itemNum
  });

  final GoodsItem itemNum;

  @override
  State<PageGoodsDetail> createState() => _PageGoodsDetailState();
}

class _PageGoodsDetailState extends State<PageGoodsDetail> {

  String searchValue = '';
  final List<String> _suggestions = ['귀걸이', '목걸이', '반지', '팔찌', '손목 시계', '브로치', '부토니에르', '벨트', '머리띠'];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
        EasySearchBar(
            title: const Text('Example'
              ,style:TextStyle
                (color: Colors.white,fontSize: 25,fontFamily: 'Roboto', fontWeight:FontWeight.w200),
            ),
            backgroundColor: Colors.brown,
            onSearch: (value) => setState(() => searchValue = value),
            actions: [
              IconButton(icon: const Icon(Icons.person), onPressed: () {})
            ],
            iconTheme: IconThemeData(color: Colors.white),
            suggestions: _suggestions
        ),drawer: Drawer(
        child: ListView(
            padding: EdgeInsets.zero,
            children: [
              Container( height: 100,
                child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.brown,
                ),
                child: Text('Drawer Header'),
              )),
              ListTile(
                  title: Text('Item 1'),
                  onTap: () => Navigator.pop(context)
              ),
              ListTile(
                  title: Text('Item 2'),
                  onTap: () => Navigator.pop(context)
              )
            ]
        )
    ),
        body:_buildBody(context),
        bottomNavigationBar:BottomNavigationBar( selectedItemColor: Colors.black,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home_max_outlined), label:'Home',  ),
            BottomNavigationBarItem(icon: Image.asset('assets/shoppingcart.jpg',width: 35,height: 35), label: 'cart' ),
            BottomNavigationBarItem(icon: Image.asset('assets/ordericon.jpg',width: 35, height: 35), label: 'order')
          ],
        ) ,);
  }



  Widget _buildBody(BuildContext context){
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            Container(
              child: Center(
                child: Text(' cold, smooth & tasty.',
                    style:TextStyle(
                        fontSize: 25,
                        fontFamily: 'Lemon',
                        color: Colors.black,
                        fontWeight: FontWeight.w500
                    )),
              ),
            ),
            Container( margin: EdgeInsets.only(top: 10),
              child: Image.asset('assets/sale.png', width: 260),
            ),
            Container(  margin: EdgeInsets.only(top: 25),
              child: Image.asset('${widget.itemNum.thumbnailImgUrl}'),
            ),
            Container( margin: EdgeInsets.only(top: 20),
              child: Column( mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('${widget.itemNum.goodsName}',
                    style: TextStyle(
                      fontFamily: 'Lemon',
                      fontSize: 18,
                      fontWeight: FontWeight.w400
                    ) ,),
                  Text('${widget.itemNum.goodsDetail}')
                ],
              ),
            ),
            Container( margin: EdgeInsets.only(top: 5),
              child: Row(mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(  margin: EdgeInsets.only(right: 5),
                    child:
                  Text('${widget.itemNum.goodsSalePercent}%',
                  style: TextStyle(
                    color: Colors.orange,
                    fontWeight: FontWeight.w400,
                    fontSize: 20
                  ))),
                  Container(
                  child:
                  Text('${widget.itemNum.goodsPrice}',
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 20
                      )))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
