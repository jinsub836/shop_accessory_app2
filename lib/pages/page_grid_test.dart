import 'package:flutter/material.dart';
import 'package:shop_accessory_app/component/component_goods_item.dart';
import 'package:shop_accessory_app/model/goods_item.dart';

class GridviewPage extends StatefulWidget {
  const GridviewPage({Key? key}) : super(key: key);

  @override
  _GridviewPageState createState() => _GridviewPageState();
}

class _GridviewPageState extends State<GridviewPage> {

  List<GoodsItem> _list = [
    GoodsItem(1, 'assets/goodsring.png', '목걸이','ㅇ', 20, 36000),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('GridviewPage'),
        ),
        body: Container( width: 600 ,
          child:
        GridView.builder(
          itemCount: _list.length, //item 개수
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, //1 개의 행에 보여줄 item 개수
            childAspectRatio: 2 / 3, //item 의 가로 1, 세로 2 의 비율
            mainAxisSpacing: 1, //수평 Padding
            crossAxisSpacing: 1, //수직 Padding
          ),
          itemBuilder: (BuildContext context, int index) {
            //item 의 반목문 항목 형성
            return ComponentGoodsItem(
                goodsItem: _list[index],
                callback: (){});
          },
        )),
    );
  }
}