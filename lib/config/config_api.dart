import 'dart:convert';
import 'package:shop_accessory_app/model/goods_item.dart';
import 'package:http/http.dart' as http;

const String base='http://localhost:8080/v1/accessorises-goods/all';

class ApiService{
final String apiUri = 'http://localhost:8080/v1';
final String base='http://localhost:8080/v1/accessorises-goods/all';

Future<List<GoodsItem>> getList() async {
  final response = await http.get(Uri.parse(base));
  if (response.statusCode == 200) {
    final List<GoodsItem> list = (jsonDecode(response.body) as List)
        .map((item) => GoodsItem.fromJson(item))
        .toList();
    return list;
  } else {
    throw Exception('Failed to load post list');
    }
  }
}
