import 'package:flutter/material.dart';
import 'package:shop_accessory_app/model/goods_item.dart';

class ComponentGoodsItem extends StatelessWidget {
  const ComponentGoodsItem({
    super.key,
    required this.goodsItem,
    required this.callback

  });

  final GoodsItem goodsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap: callback,
      child: Container(
        child:
        Column(
          children: [
            Container( height: 190, width: 180,
              margin: EdgeInsets.only(top: 30),
              child:
              Image.asset('${goodsItem.thumbnailImgUrl}'),
            ),
            Container(
              child:
                  Center(
              child: Text(goodsItem.goodsName,
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                fontFamily: 'Lemon'
              )),
            )),
            Container( margin: EdgeInsets.only(top:5),
              child:
            Row(mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('${goodsItem.goodsSalePercent}%',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.orange,
                        fontWeight: FontWeight.w400
                    )),
                Text('${goodsItem.goodsPrice}',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400
                    ))
              ],
            ))],
        ),),
    );
  }
}
